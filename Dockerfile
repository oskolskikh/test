FROM php:8-fpm

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/

RUN install-php-extensions opcache zip xdebug memcached pdo_mysql

RUN apt-get update && apt-get install -y mc nginx runit memcached

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin
RUN mv /bin/composer.phar /bin/composer

RUN rm -rf /etc/nginx/site-available
RUN rm -rf /etc/nginx/sites-enabled
COPY etc/ /etc/
RUN nginx -t

WORKDIR /var/www/test
COPY test/ /var/www/test

RUN composer install --working-dir=/var/www/test/




