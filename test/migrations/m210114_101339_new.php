<?php

use yii\db\Migration;

/**
 * Class m210114_101339_new
 */
class m210114_101339_new extends Migration
{

    public function up()
    {
        $this->createTable(
            'currency', [
                'id' => $this->primaryKey(),
                'name' => $this->string(3)->notNull()->unique(),
                'last' => $this->float(2)->defaultValue(0),
                'buy' => $this->float(2)->defaultValue(0),
                'sell' => $this->float(2)->defaultValue(0),
                'updated_at' => $this->timestamp()
                    ->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE NOW()'),
            ]
        );
    }

    public function down()
    {
        $this->dropTable('currency');
        return true;
    }

}
