<?php

namespace app\controllers;

use app\service\PriceService;
use Yii;
use yii\db\Exception;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

/**
 * Class ApiController
 * @package app\controllers
 */
class ApiController extends Controller
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => HttpBearerAuth::class,
            ]
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        try {
            parent::beforeAction($action);
        } catch (UnauthorizedHttpException $e) {
            Yii::$app->response->setStatusCode(403);
            throw new UnauthorizedHttpException('Invalid token');
        }
        return true;
    }

    /**
     * @return array|string[]
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            Yii::$app->response->statusCode = 404;
            return ['message' => $exception->getMessage()];
        }
        Yii::$app->response->statusCode = 404;
        return ['message' => 'Bad query'];

    }

    /**
     * @param string $parameter
     * @return array
     * @throws Exception
     */
    public function actionRates($parameter = '')
    {
        if (!$parameter) {
            return PriceService::getRates();
        }
        return PriceService::getRatesByCurrency($parameter);
    }

    /**
     * @param $currency_from
     * @param $currency_to
     * @param $value
     * @return array
     * @throws Exception
     */
    public function actionConvert($currency_from, $currency_to, $value)
    {
        if ($currency_from === 'BTC' || $currency_to === 'BTC') {
            return PriceService::convertAction($currency_from, $currency_to, $value);
        }
        throw new Exception('Not implemented');
    }
}
