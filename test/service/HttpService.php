<?php

namespace app\service;

use Yii;
use yii\httpclient\Client;

class HttpService
{
    /**
     * @var Client
     */
    protected $client;

    public function getClient()
    {
        if (empty($this->client)) {
            $this->client = new Client([
                    'responseConfig' => [
                        'format' => Client::FORMAT_JSON
                    ]]
            );
        }
        return $this->client;
    }

    public static function send()
    {
        $url = Yii::$app->params['price_url'];
        $client = new HttpService();
        return $client->getClient()->createRequest()->setMethod('get')
            ->setUrl($url)
            ->send()->getContent();
    }
}
