<?php

namespace app\service;

use app\models\Currency;
use Yii;
use yii\caching\MemCache;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * Class PriceService
 * @package app\service
 */
class PriceService
{
    /**
     * @var array
     */
    protected array $curency = [];

    /**
     *
     */
    public static function exec()
    {
        $oClass = new self();
        $oClass->getPriceFromUrl();
    }

    /**
     * @return $this
     */
    public function getPriceFromUrl()
    {
        try {
            $priceData = HttpService::send();
            $content = Json::decode($priceData);
            if ($content) {
                return $this->store($content);
            }
            throw new Exception('Empty Body');
        } catch (\Exception $e) {
            //@toDo log this
        } catch (\Error $error) {
            //@toDo log fatal error
        }
    }

    /**
     * @param $content
     * @return $this
     */
    private function store($content)
    {
        foreach ($content as $key => $value) {
            $oCurrencyModel = Currency::findOne(['name' => $key]);
            if (!$oCurrencyModel) {
                $oCurrencyModel = new Currency();
                $oCurrencyModel->name = $key;
            }
            $oCurrencyModel->sell = (float)$value['sell'] ?? '';
            $oCurrencyModel->buy = (float)$value['buy'] ?? '';
            $oCurrencyModel->last = (float)$value['last'] ?? '';
            $oCurrencyModel->save();
        }
        return $this;
    }

    /**
     * @return array
     */
    public static function getCurrency(): array
    {
        $result = [];
        $aCurrency = Currency::find()->select('name')->asArray()->all();
        foreach ($aCurrency as $key => $val) {
            $result[] = $val['name'];
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getRates(): array
    {
        $aCurrency = Currency::find()->select('name,sell,buy')->asArray()->orderBy(['sell' => SORT_DESC])->all();
        foreach ($aCurrency as $key => $val) {
            $aCurrency[$key] = self::commission($aCurrency[$key]);
        }
        return $aCurrency;
    }

    /**
     * @param $name
     * @return array
     * @throws Exception
     */
    public static function getRatesByCurrency($name)
    {
        $aCurrency = Currency::find()->select('name,sell,buy')->asArray()->where(['name' => $name])->orderBy(['sell' => SORT_DESC])->one();
        if ($aCurrency) {
            return self::commission($aCurrency);
        }
        throw new Exception('Not Found');
    }

    /**
     * @param array $value
     * @return array
     */
    public static function commission(array $value): array
    {
        $value['sell'] = round(($value['sell'] - ($value['sell'] / 100 * Yii::$app->params['commission'])), 2);
        $value['buy'] = round(($value['buy'] - ($value['buy'] / 100 * Yii::$app->params['commission'])), 2);
        return $value;
    }

    /**
     * @param $from
     * @param $to
     * @param $value
     * @return array
     * @throws \Exception
     */
    public static function convertAction($from, $to, $value): array
    {
        if ($from === 'BTC' && $value < 0.0000005556) {
            throw new Exception('Denied');
        }
        if ($to === 'BTC' && $value < 0.01) {
            throw new Exception('Denied');
        }
        if ($from === 'BTC') {
            return self::convertFromBTC($value, $to);
        }
        if ($to === 'BTC') {
            return self::convertToBTC($value, $from);
        }
        return [];
    }

    /**
     * @param $value
     * @param $to
     * @return array
     * @throws \Exception
     */
    public static function convertFromBTC($value, $to)
    {
        $oCurrency = Currency::findOne(['name' => $to]);
        if (!$oCurrency) {
            throw new \Exception('Not implemented');
        }
        $dValue = $value * $oCurrency->sell;
        $dValue = round($dValue - ($dValue * (Yii::$app->params['commission'] / 100)), 2);

        return [
            'currency_from' => 'BTC',
            'currency_to' => $to,
            'value' => $value,
            'converted_value' => $dValue,
            'rate' => (float)Yii::$app->params['commission'],
        ];
    }

    /**
     * @param $value
     * @param $to
     * @return array
     */
    public static function convertToBTC($value, $to)
    {
        $oCurrency = Currency::findOne(['name' => $to]);
        if (!$oCurrency) {
            throw new \Exception('Not implemented');
        }
        $dValue = $value / $oCurrency->buy;
        $dValue = round($dValue - ($dValue * (Yii::$app->params['commission'] / 100)), 10);
        return [
            'currency_from' => $to,
            'currency_to' => 'BTC',
            'value' => $value,
            'converted_value' => $dValue,
            'rate' => (float)Yii::$app->params['commission'],
        ];
    }
}
