<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'mgbcvQhKXJlYj6ny64_B0d2hPgrsZAeL',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ],
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                $data = $response->data;
                $response->data = [
                    'code' => $response->statusCode,
                ];
                if ($response->isSuccessful) {
                    $response->data['success'] = 'success';
                    $response->data['data'] = $data;
                } else {
                    $response->data['success'] = 'error';
                    $response->data['message'] = $data['message'];
                }

            },
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'memcache' => [
            'class' => 'yii\caching\MemCache',
            'useMemcached' => true,
            'servers' => [
                [
                    'host' => 'localhost',
                    'port' => 11211,
                ]
            ]
        ],
        'user' => [
            'identityClass' => 'app\models\TestUser',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'api/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'GET api/v2/rates' => 'api/rates',
                'POST api/v2/convert' => 'api/convert',
                '/api/v2' => 'api/error',
            ],
        ]
    ],
    'params' => $params,
];

return $config;
