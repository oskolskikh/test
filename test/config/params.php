<?php

return [
    'adminEmail' => 'admin@example.com',
    'price_url' => 'https://blockchain.info/ticker',
    'commission' => 2,
];
