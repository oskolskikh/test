<?php
$dbname = getenv('TEST_MYSQL_DATABASE');
$host = getenv('TEST_MYSQL_HOST');
$username = getenv('TEST_MYSQL_USER');
$password = getenv('TEST_MYSQL_PASSWORD');

return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host={$host};dbname={$dbname}",
    'username' => $username,
    'password' => $password,
    'charset' => 'utf8',
];
