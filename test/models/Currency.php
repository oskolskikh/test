<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property string $name
 * @property float $last
 * @property float $buy
 * @property float $sell
 * @property string $updated_at
 * Class Currency
 * @package app\models
 */
class Currency extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'currency';
    }
}
