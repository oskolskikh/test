<?php

namespace app\commands;

use app\service\PriceService;
use yii\console\Controller;
use yii\console\ExitCode;

class RefreshController extends Controller
{
    public function actionIndex($message = 'hello world')
    {
        PriceService::exec();
        return ExitCode::OK;
    }

    public function actionCurrency()
    {
        $currency = PriceService::getCurrency();
        echo "Available currency: " . implode(',', $currency) . PHP_EOL;
        return ExitCode::OK;
    }

    public function actionRates()
    {
        $rates = PriceService::getRates();
        return ExitCode::OK;
    }

    public function actionConvert()
    {
        $convert = PriceService::convertAction('USD', 'BTC', 100000);
        var_dump($convert);
        return ExitCode::OK;
    }
}
