<?php
define('YII_DEBUG', getenv('YII_DEBUG'));
define('YII_ENV', getenv('YII_ENV'));

if (YII_ENV !== 'prod') {
    define('YII_ENV_DEV', true);
}

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();

