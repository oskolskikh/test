Для возраста лучше использовать дату рождения ,сделал как было указано в задании


CREATE TABLE `books` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(50) NOT NULL,
 `author` varchar(50) NOT NULL,
 PRIMARY KEY (`id`)
) 
CREATE TABLE `user` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `first_name` varchar(20) NOT NULL,
 `last_name` varchar(20) NOT NULL,
 `age` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) 

CREATE TABLE `user_books` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `books_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `user_id` (`user_id`),
 KEY `books_id` (`books_id`)
)

Задание:

SELECT concat(`user`.`first_name`, ' ', `user`.`last_name`) as Name, `books`.`author` as Author, GROUP_CONCAT(`books`.`name`) as Books FROM `user_books`
    LEFT JOIN `books` on `user_books`.`books_id` = `books`.`id`
    LEFT JOIN `user` on `user`.`id`=`user_books`.`user_id`
WHERE `user`.age  BETWEEN 7 and 17
GROUP BY `user_books`.`user_id`
HAVING COUNT(`books`.`name`)=2 AND COUNT(DISTINCT books.author)=1

ИЛИ по старинке

SELECT  `books`.`author` as Author,
                concat(`user`.`first_name`, ' ', `user`.`last_name`) as Name,
                GROUP_CONCAT(`books`.`name`) as Books FROM `user_books`
            LEFT JOIN `books` on `user_books`.`books_id` = `books`.`id`
            LEFT JOIN `user` on `user`.`id`=`user_books`.`user_id`
            INNER JOIN (
                SELECT  user_books.user_id,books.author, count(*) AS total_books,COUNT(DISTINCT books.author) as book_author from user_books
                	left JOIN books on user_books.books_id = books.id
                GROUP BY user_id
            ) as c ON c.user_id= `user_books`.`user_id`
             WHERE `user`.age BETWEEN 7 and 17 AND c.total_books=2 AND c.book_author=1 GROUP BY user_books.user_id
